
// Handle sort by first letter
export const sortByLetter =(self, newState)=>{
    let newArr = [...newState];
    // Sort A -> Z first letter
    
    if(self === "A-Z"){
        return newArr.sort((a,b)=> a.name[0].toUpperCase() > b.name[0].toUpperCase() ? 1:-999);
        
    }
    // Sort Z -> A first letter
    if(self === "Z-A"){
        return newArr.sort((a,b)=> a.name[0].toUpperCase() > b.name[0].toUpperCase() ? -999:1);
    }
  
}
// Handle filter type of phone when user choose.
export const filterType = (self, newState) =>{
    let newArr = [...newState];

    // filter by apple xiaomi samsung oppo vsmart

    if(self === "apple"){
        return newArr.filter(el => el.type === self);
    }
    if(self === "xiaomi"){
        return newArr.filter(el => el.type === self);
    }
    if(self === "samsung"){
        return newArr.filter(el => el.type === self);
    }
    if(self === "oppo"){
        return newArr.filter(el => el.type === self);
    }
    if(self === "vsmart"){
        return newArr.filter(el => el.type === self);
    }
   
}