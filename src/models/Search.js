export default class Search {
    async getData(){
        try {
            const res = await fetch(`https://5f2a9bd76ae5cc0016422c69.mockapi.io/product`)
            this.data = await res.json();
            return this.data;
          
        } catch (error) {
            alert("Loi ket noi");
        }
    }
    findMatches(wordToMatch, newResult) {

        return newResult.filter(product => {
            const regex = new RegExp(wordToMatch, 'gi');
            return product.name.match(regex);
          });
    }
    
}