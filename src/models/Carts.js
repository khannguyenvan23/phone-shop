import Product from "./Product.js";

export default class Carts{
    constructor() {
        this.carts = [];
        this.totalPrice = 0;
    }

   
    addProduct(product) {
       
        this.carts.push(product);
        this.persistData();
        // Perist data in localStorage
        // this.persistData();

    }
    resetCarts(){
        this.carts = [];
        this.totalPrice = 0;
        this.persistData();
    }
    deleteProduct(id) {
        const index = this.carts.findIndex(el => el.id === id);
        this.carts.splice(index, 1);
        this.persistData();
        // Perist data in localStorage
        // this.persistData();
    }

    isProduct(id) {
        return this.carts.findIndex(el => el.id === id) !== -1;
    }
    getProduct(id){
        return this.carts.find(el => el.id === id);
    }
    replaceProduct(id, product){
        const index = this.carts.findIndex(el => el.id === id);
        this.carts.splice(index, 1, product);
        this.persistData();
    }
    getNumProduct() {
        return this.carts.length;
    }
    caculatePrice(){
        if(this.carts.length > 1){
            this.totalPrice = this.carts.reduce((acc, curr)=>{
                acc += curr.getTotalPrice()
                return acc;
            }, 0);
        } else if(this.carts.length > 0){
            this.totalPrice = this.carts[0].getTotalPrice();
            
        } else {
            this.totalPrice = 0;
           
        }
        this.persistData();
       
    }
    persistData() {
        localStorage.setItem('CarstData', JSON.stringify(this.carts));
        localStorage.setItem('TotalPrice', JSON.stringify(this.totalPrice));
    }
    // Read data to localStorage
    readStorage() {
        const storageCarts = JSON.parse(localStorage.getItem('CarstData'));
        
        const storageTotalPrice = JSON.parse(localStorage.getItem('TotalPrice'));
        // Restoring data from the localStorage
        if (storageCarts){
            storageCarts.map(el=>el.__proto__ = new Product())
            this.carts = storageCarts;
        } 
        if (storageTotalPrice) this.totalPrice = storageTotalPrice;
    }
    
    
}
