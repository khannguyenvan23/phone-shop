
export default class Product {
    constructor(id){
        this.id = id;
    }
    async getData() {
        try {
            const res = await fetch(`https://5f2a9bd76ae5cc0016422c69.mockapi.io/product/${this.id}`);
            const data = await res.json();
            this.name = data.name;
            this.price = data.price;
            this.image = data.image;
            this.inventory = data.inventory;
            // this.amount = 1;
            this.totalPrice = this.price;

            
        } catch (error) {
           alert("Cần phải có kết nối mạng mới sử dụng được chức năng này")
        }
        
    }
    // Update amount when + - 
    updateAmount (type) {
        // Servings
        let newAmount = this.amount;
        if(type === 'down'){
            newAmount = this.amount - 1;
        }else {
            if(newAmount < this.inventory){
                newAmount = this.amount + 1;
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Hết hàng!',
                    text: `${this.name} không đủ số lượng.`,
                    showConfirmButton: true,
                    timer: 2000
                  });
            }
      
        }
        this.totalPrice = newAmount * this.price;
        this.amount = newAmount;
       
    }
    getTotalPrice(){
        return this.totalPrice;
    }
    setAmount(amount){
        this.amount = amount;
        this.totalPrice = this.amount * this.price;
    }
    getAmount() {
        return this.amount;
    }
    

}