

export default class Home {
   
    // Get data from sever 
    async getData() {
        try {
            const res = await fetch(`https://5f2a9bd76ae5cc0016422c69.mockapi.io/product`)
            this.data = await res.json();
        } catch (error) {
            console.log(error);
        }
       
        // Call function to save data to localStorage
        this.persistData();
        return this.data;
    }
    // Save data to localStorage
    persistData() {
        localStorage.setItem('homeData', JSON.stringify(this.data));
    }
    // Read data to localStorage
    readStorage() {
        const storage = JSON.parse(localStorage.getItem('homeData'));
        // Restoring data from the localStorage
        if (storage) this.data = storage;
    }
   

}