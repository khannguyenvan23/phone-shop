import {DOMString as DOM, numberWithCommas} from './Base.js'

// Clear all product in home
export const clearCards = () => {
    DOM.cards.innerHTML = "";
}
// Render all product in home
export const renderAllProduct = (state) =>{
    state.forEach(product => {
        // render each element to home
        renderProduct(product);
       });
    
}


// function render product
const renderProduct = (product) =>{
    // Get location to display result.
     const cards = DOM.cards;
    // Check if product is real
    if(!product) return
    // Unpack Product
    const {id, name, image, price, inventory, description} = product;

    // format price vd 7.99 to this 7.900.000 vnd
    const newPrice = numberWithCommas(price);
    // Create String template 
    const html = `
           <figure class="card" data-id="${id}">
                <div class="card__hero">
                    <img src="${image}" alt="" class="card__img">
                </div>
                <h2 class="card__name">${name}</h2>
                <p class="card__des">${description}</span>
                <p class="card__price">
                         
                        <span class="emoji-left">${newPrice}₫</span>

                         <span>Còn lại: ${inventory} cái</span>
                </p>
                <div class="updown">
                    <div class="down"><span></span></div>
                    <input class="number" data-amount="${id}" value="1" maxlength="50" pattern="[0-9]*" type="number">
                    <div class="up"><span></span><span></span></div>
                </div>
                <div class="card__footer">
                    <button class="card__link add__cart" data-id="${id}">
                        <span class="emoji-left">👉</span> Thêm vào giỏ hàng
                    </button>
                </div>
           </figure>
    `

    
    // Append new template to location 
    cards.insertAdjacentHTML('beforeend', html);
   
}

