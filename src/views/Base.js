export const DOMString = {
    cards: document.querySelector(".cards-container"),
    cardAmount: document.querySelector(".number"),
    filterTypePhone: document.getElementById("filterTypePhone"),
    sortFisrtWord: document.getElementById("sortFisrtWord"),
    resetFilter: document.getElementById("resetFilter"),
    navShopAmount: document.querySelectorAll(".nav__shop-amount"), 
    shopping: document.querySelector(".lst-product__container"),
    productpay: document.querySelector(".lst-product__pay"),
    cartspay: document.querySelector(".carts__pay"),
    search: document.querySelector(".search")

}


export const elementStrings = {
    loader: 'lds-ring',
    bccsp: 'bccsp',
};
// display nut thanh toan
export const displayThanhToan = () => {
    //when khi co san pham
    DOMString.productpay.style.display = "block";
   
}
export const clearThanhToan = () => {
     // An thanh toan san pham khi khong co san pham nao
     DOMString.productpay.style.display = "none";
}

// Handle loading get result from API
export const renderLoader = parent => {
    const loader = `
        <div class="${elementStrings.loader}"><div></div><div></div><div></div><div></div></div>
    `;
    parent.insertAdjacentHTML('beforebegin', loader);
};

export const clearLoader = () => {
    const loader = document.querySelector(`.${elementStrings.loader}`);
    if (loader) loader.parentElement.removeChild(loader);
};

// Handle filter display

export const displayFilter = _ =>{
    document.querySelector(".filter").style.display = "flex";
}

// Handle figure

export const displayCart = (id, amount) =>{
    const selected = document.querySelector(`[data-id="${id}"]`);
    selected.children[4].style.display = "block";
    
    selected.children[5].style.display = "none";
   
    if(amount) document.querySelector(`[data-amount="${id}"]`).value = amount;
}
export const displayAdd = (id) =>{
    const selected = document.querySelector(`[data-id="${id}"]`);
    selected.children[4].style.display = "none";
    
    selected.children[5].style.display = "flex";
}
 
// Reset nav shop amount 
// format currency vnđ
export const numberWithCommas = (x) => {
    if (isNaN(x)) {
        return '0';
     }
    let parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    return parts.join(".");
}
export const renderBccsp = () => {
    const bccsp = `
        <p style="text-align: center" class="${elementStrings.bccsp}">Bạn chưa có sản phẩm nào</p>
    `;
    DOMString.shopping.insertAdjacentHTML('beforeend', bccsp);
};
export const clearBccsp = () => {
    const bccsp = document.querySelector(`.${elementStrings.bccsp}`);
    if (bccsp) bccsp.parentElement.removeChild(bccsp);
};