import {DOMString as DOM, numberWithCommas} from './Base.js'

export const renderAmount = (amount, id) => {
   document.querySelector(`[data-amount="${id}"]`).value = amount;
}

export const renderNavShop = (totalPrice, totalAmount = 0) =>{
   //Get dom
   const totalAmountDOM = DOM.navShopAmount[0];
   const totalPriceDOM = DOM.navShopAmount[1];
   const newTotalPrice = totalPrice;
   const newPrice = numberWithCommas(newTotalPrice);
   // Render when amount and price exist
   
   if(totalAmount > 0) {
      totalAmountDOM.textContent = totalAmount;
      
   }else if(totalAmount === -1) {
      totalAmount = 0;
      totalAmountDOM.textContent = totalAmount;
   }
   totalPriceDOM.textContent = newPrice + "₫";
   document.querySelector(".tong-thanh-toan strong").textContent = newPrice + "₫";

  
   
}
export const renderAmountCarts = (amount, id) => {
   document.querySelector(`[data-nokia="${id}"]`).value = amount;
}

export const renderPriceCarts = (totalprice, id) => {
   const newTotalPrice = numberWithCommas(totalprice);
   document.querySelector(`[data-totalprice="${id}"]`).textContent = newTotalPrice + "₫";
}
// Render lst-product
// function render product

export const renderItem = item => {
   let {id, name, image, totalPrice, amount } = item;
   if(!amount) amount = 1;
    
   const newTotalPrice = numberWithCommas(totalPrice);
   const markup = `
      <div data-code="${id}" class="carts___item">
         <div class="carts___item-img">
            <img src="${image}" width="60" height="60"> 
         </div>
      
         <div class="carts___item-info">
            <p>${name}</p>
            <div class="control-item">
               <div class="quantitynum">
                     <button class="des">-</button> <input type="number" data-nokia="${id}" value="${amount}"> <button class="ins">+</button>
               </div> 
               <button class="delete">Xóa</button> 
            </div>
         </div>
         <div class="carts___item-colmoney" data-totalprice="${id}"><strong>${newTotalPrice}₫</strong>
         </div>
      </div>
   `;
   DOM.shopping.insertAdjacentHTML('beforeend', markup);
};

export const deleteItemCarts = id => {
   const item = document.querySelector(`[data-code="${id}"]`);
   if (item) item.parentElement.removeChild(item);
};
