import {DOMString as DOM, numberWithCommas} from './Base.js'

export const displayMatches = (value, matchArray) => {
    const html = matchArray.map(product => {
      const regex = new RegExp(value, 'gi');
      const {id, image, inventory, description, price} = product;
       const nameFind = product.name.replace(regex, `<span class="hl">${value}</span>`);
     

    // format price vd 7.99 to this 7.900.000 vnd
    const newPrice = numberWithCommas(price);
      return `
            <figure class="card" data-id="${id}">
            <div class="card__hero">
                <img src="${image}" alt="" class="card__img">
            </div>
            <h2 class="card__name">${nameFind}</h2>
            <p class="card__des">${description}</span>
            <p class="card__price">
                    
                    <span class="emoji-left">${newPrice}₫</span>

                    <span>Còn lại: ${inventory} cái</span>
            </p>
            <div class="updown">
                <div class="down"><span></span></div>
                <input class="number" data-amount="${id}" value="1" maxlength="50" pattern="[0-9]*" type="number">
                <div class="up"><span></span><span></span></div>
            </div>
            <div class="card__footer">
                <button class="card__link add__cart" data-id="${id}">
                    <span class="emoji-left">👉</span> Thêm vào giỏ hàng
                </button>
            </div>
        </figure>
      `;
    }).join('');

    
    // Create String template 
   
    DOM.cards.innerHTML = html;
  }