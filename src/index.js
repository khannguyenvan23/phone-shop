import Home from './models/Home.js'
import Product from './models/Product.js'
import { clearCards, renderAllProduct } from './views/homeView.js'
import {DOMString as DOM, renderLoader, clearLoader, displayFilter, displayCart, renderBccsp, clearBccsp, displayAdd, displayThanhToan,clearThanhToan, DOMString } from './views/Base.js'
import { sortByLetter,  filterType} from './models/Filter.js'
import Carts  from './models/Carts.js'
import { renderAmount, renderNavShop, renderItem as renderItemShop,renderAmountCarts, renderPriceCarts, deleteItemCarts } from './views/cartsView.js'
import  Search  from './models/Search.js'
import { displayMatches } from './views/searchView.js'
/**
 * Global variable
 * - Contain all data from API
 */
let state = {};
let newState;

/**
 * HOME CONTROLLER
 */
// Handle all Result from API and applying the  result to the webpages
const homeConstroller = async() => {
    const productData = new Home();
   // Clear all result of Cards
    clearCards();
     //  Add loading before get result
    renderLoader(DOM.cards);

    
    try {
        // if old data is exist load this data 
        productData.readStorage();
        // Get data from server
        state.data = await productData.getData();
        
        newState = [...state.data];
         // Render all result from API to home
         renderAllProduct(state.data);
         displayFilter();
         // Clear loading affter render all data
        clearLoader();
    } catch (error) {
        clearLoader();
        alert("Lỗi" + error);
    }
    
}
// Sort the object's values by first letter.
const filterCtrl = (self, typeDOM) => {
   
    // Clear all result of Cards
    clearCards();
     //  Add loading before get result
    renderLoader(DOM.cards);

    if(self === "normal" && typeDOM === "reset"){
         // Render all result from API to home
         newState = [...state.data];
         renderAllProduct(state.data);
         state.carts.readStorage();
         state.carts.carts.forEach(element => {
            displayCart(element.id, element.amount);
        });
         // Clear loading affter render all data
        clearLoader();
        
        return
    }
    // Fix use 2 filter the same time
    if(typeDOM === "sort"){
        
         newState = sortByLetter(self, newState);
    }
    if(typeDOM === "filter"){
        
        newState = filterType(self, newState);
   }
   

    // Render all result from API to home
    state.carts.readStorage();
    const newStateNew  = newState.filter((elState)=>{
        for(let i = 0; i < state.carts.carts.length;i++ ){
            if(elState.id === state.carts.carts[i].id){
                return state.carts.carts[i];
            }
        }
      
    })
       
  
    renderAllProduct(newState);
    console.table(newStateNew);
    newStateNew.forEach(element => {
        displayCart(element.id, element.amount);
    });
   
    // Clear loading affter render all data
   clearLoader();
}
/**
 * Product CONTROLLER
 */
const productController = async (id) => {
    // Get ID from url
    

    if (!id) return

    const product = new Product(id);
    
    try {
        await product.getData();
        // Add amount to product
       
        // data.amount = firstAmount;
         state.product = product;

    } catch (error) {
        clearLoader();
        alert("Lỗi asfsajfj" + error);
    }
};
 
/** 
 * Carts CONTROLLER
 */
const cartsController = () => {
    if (!state.carts) state.carts = new Carts();

    const currentID = state.product.id;
    // Carts has NOT add yet
    if(!state.carts.isProduct(currentID)){
        if (!state.carts.isProduct(currentID)) {
            state.carts.addProduct(
                 state.product
             );
             state.carts.caculatePrice();
             renderNavShop(state.carts.totalPrice,  state.carts.getNumProduct());
             clearBccsp();
             
             renderItemShop(state.product);
        }
  
    }
};
/* ADD EVENTLISTENER */ 
/* load carts */
const loadCarts = () => {
    
    if (!state.carts) state.carts = new Carts();
    state.carts.readStorage();
    if(state.carts.getNumProduct() === 0){
        renderBccsp();
    }else {
        displayThanhToan();
        clearBccsp();
    }
    renderNavShop(state.carts.totalPrice,  state.carts.getNumProduct());
    state.carts.carts.forEach(element => {
        displayCart(element.id, element.amount);
        renderItemShop(element);
    });
   
}
// After web pages load homeConstroller is trigger

window.addEventListener("load", async () => {
    await homeConstroller();
    // Load read localStorage
    loadCarts();
     
})

// Sort firstWord A-Z Z-A after guest click selected
const sortFisrtWordDOM = DOM.sortFisrtWord;
const filterTypeDom = DOM.filterTypePhone;
const resetFilter = DOM.resetFilter;
// Handle sort
sortFisrtWordDOM.addEventListener("change", function(){
    // Because bind this in an arrow function 
    const self = this.value;
    const typeDOM = "sort";
    if(!self) return
    filterCtrl(self, typeDOM);
});
// Handle filter
filterTypeDom.addEventListener("change", function(){
    // Because bind this in an arrow function 
    newState = [...state.data];
    const self = this.value;
    const typeDOM = "filter";
    if(!self) return
    filterCtrl(self, typeDOM);
});

// Handle Reset
resetFilter.addEventListener("click", function(){
    // Because bind this in an arrow function 
    const self = this.value;
    const typeDOM = "reset";
    if(!self) return
    filterCtrl(self, typeDOM);
   sortFisrtWordDOM[0].selected = true;
   filterTypeDom[0].selected = true;
    // Handle default of sortFisrtWordDOM vs filterTypeDom
});
const controllCarts = async (e) => {
    const currentId = e.target.closest('.card').dataset.id; 
    if(currentId){
        
       
        // state.globalAmount.push([currentId, currentAmount])


        state.product = state.carts.getProduct(currentId);
        let currentAmount = parseInt(document.querySelector(`[data-amount="${currentId}"]`).value)
        if(currentAmount > state.product.inventory){
            Swal.fire({
                icon: 'error',
                title: 'Hết hàng!',
                text: `${state.product.name} không đủ số lượng.`,
                showConfirmButton: true,
                timer: 2000
              });
            document.querySelector(`[data-amount="${currentId}"]`).value = state.product.inventory;
            return
        }
       
        state.product.setAmount(currentAmount);
     
        // Calculate next price
        state.carts.replaceProduct(currentId, state.product);
        state.carts.caculatePrice();
        
        // Replace product on carts
       
        // Render amount
        renderNavShop(state.carts.totalPrice);
        renderAmount(state.product.getAmount(), currentId);
        renderAmountCarts(state.product.getAmount(), currentId);
        renderPriceCarts(state.product.totalPrice, currentId);
    }
}
const controllShopping = async (e) =>{
    const currentId = e.target.closest('.carts___item').dataset.code; 
    if(currentId){
        let currentAmount = parseInt(document.querySelector(`[data-nokia="${currentId}"]`).value)
      

        state.product = state.carts.getProduct(currentId);
        if(currentAmount > state.product.inventory){
            Swal.fire({
                icon: 'error',
                title: 'Hết hàng!',
                text: `${state.product.name} không đủ số lượng.`,
                showConfirmButton: true,
                timer: 2000
              });
            document.querySelector(`[data-nokia="${currentId}"]`).value = state.product.inventory;
            return;
        }
        state.product.setAmount(currentAmount);
     
        // Calculate next price
        state.carts.replaceProduct(currentId, state.product);
        state.carts.caculatePrice();
        
        // Replace product on carts
       
        // Render amount
        renderNavShop(state.carts.totalPrice);
        renderAmount(state.product.getAmount(), currentId);
        renderAmountCarts(state.product.getAmount(), currentId);
        renderPriceCarts(state.product.totalPrice, currentId);
    }
}
// Handling product clicks home
// DOM.cards.addEventListener('change', controllCarts);
DOM.cards.addEventListener('keyup', controllCarts);
// DOM.shopping.addEventListener('change', controllShopping);
DOM.shopping.addEventListener('keyup', controllShopping);

DOM.cards.addEventListener('click', async e => {

    const currentId = e.target.closest('.card').dataset.id;
   
    if(!currentId) return
   
    if (e.target.matches('.down, .down *')) {
        if(state.carts.isProduct(currentId)){
            state.product = state.carts.getProduct(currentId);
            if(state.product.getAmount() > 0){
                console.log(currentId);
                // state.product.setAmount(currentAmount);
                const firstAmount = parseInt(document.querySelector(`[data-amount="${currentId}"]`).value);
                
              
                state.product.setAmount(firstAmount);
                state.product.updateAmount('down');

                // Calculate next price

                state.carts.caculatePrice();
                
                // Replace product on carts
                state.carts.replaceProduct(currentId, state.product);
                
                renderNavShop(state.carts.totalPrice);
                // Render amount
                renderAmount(state.product.getAmount(), currentId);
                renderAmountCarts(state.product.getAmount(), currentId);
                renderPriceCarts(state.product.totalPrice, currentId);

            }
           
        }
       
    } else if (e.target.matches('.up, .up *')) {
        if(state.carts.isProduct(currentId)){
            // state.product.setAmount(currentAmount)
            const firstAmount = parseInt(document.querySelector(`[data-amount="${currentId}"]`).value);
            state.product = state.carts.getProduct(currentId);
            state.product.setAmount(firstAmount);
             state.product.updateAmount('up');

             // Calculate next price

             state.carts.caculatePrice();
               // Replace product on carts
               state.carts.replaceProduct(currentId, state.product);
             renderNavShop(state.carts.totalPrice);
           
            // Render amount
             renderAmount(state.product.getAmount(), currentId);
             renderAmountCarts(state.product.getAmount(), currentId);
             renderPriceCarts(state.product.totalPrice, currentId);
        }
      
       
    } else if (e.target.matches('.add__cart, .add__cart *')) {
     
       // create new obj Product base on id
       await productController(currentId);
      // Díplay popup "Đã thêm vào giỏ hàng";
      Swal.fire({
        icon: 'success',
        title: 'Updated!',
        text: ` ${state.product.name} đã được thêm vào giỏ hàng.`,
        showConfirmButton: false,
        timer: 1500
      });
      displayThanhToan();
       // display none add__cart
       // display block data-updown
       displayCart(currentId);
       // Update carts result base on carts arr length
       cartsController();
     
       //
       
        
    }
    // console.clear();
   
    
    
});
// Handling product clicks carts

DOM.shopping.addEventListener('click', async e => {

    const currentId = e.target.closest('.carts___item').dataset.code;
    if(!currentId) return
   
    if (e.target.matches('.des, .des *')) {
        if(state.carts.isProduct(currentId)){
            if(state.product.getAmount() > 0){
                // state.product.setAmount(currentAmount);
                const firstAmount = parseInt(document.querySelector(`[data-amount="${currentId}"]`).value);
                state.product = state.carts.getProduct(currentId);
                state.product.setAmount(firstAmount);
                state.product.updateAmount('down');

                // Calculate next price

                state.carts.caculatePrice();
                
                // Replace product on carts
                state.carts.replaceProduct(currentId, state.product);
                renderNavShop(state.carts.totalPrice);
                // Render amount
                renderAmount(state.product.getAmount(), currentId);
                // Render carts amount vs total price each item
                renderAmountCarts(state.product.getAmount(), currentId);
                renderPriceCarts(state.product.totalPrice, currentId);
            }
           
        }
       
    } else if (e.target.matches('.ins, .ins *')) {
        if(state.carts.isProduct(currentId)){
            // state.product.setAmount(currentAmount)
            const firstAmount = parseInt(document.querySelector(`[data-amount="${currentId}"]`).value);
            state.product = state.carts.getProduct(currentId);
            state.product.setAmount(firstAmount);
             state.product.updateAmount('up');

             // Calculate next price

             state.carts.caculatePrice();
            
             // Replace product on carts
             state.carts.replaceProduct(currentId, state.product);
             renderNavShop(state.carts.totalPrice);
            // Render amount
             renderAmount(state.product.getAmount(), currentId);
              // Render carts amount vs total price each item
              renderAmountCarts(state.product.getAmount(), currentId);
              renderPriceCarts(state.product.totalPrice, currentId);
              
        }
      
       
    }else if (e.target.matches('.delete, .delete *')) {
        if(state.carts.isProduct(currentId)){
            state.carts.deleteProduct(currentId);
            deleteItemCarts(currentId);
            displayAdd(currentId);
            // reset this amount of product
            document.querySelector(`[data-amount="${currentId}"]`).value = 1;
            if(state.carts.getNumProduct() > 0){
                state.carts.caculatePrice();
                renderNavShop(state.carts.totalPrice,  state.carts.getNumProduct());
            }else {
                state.carts.caculatePrice();
                renderNavShop(state.carts.totalPrice, -1);
                renderBccsp();
                clearThanhToan();
            }
            // Replace product on carts
        
            
           
        }
      
       
    }
    
});

DOM.cartspay.addEventListener('click', ()=>{
    
    if(state.carts.carts.length > 0 ){
        state.carts.carts.forEach(el=>{
            document.querySelector(`[data-amount="${el.id}"]`).value = 1;
            displayAdd(el.id);
        })
        state.carts.resetCarts()
       
        DOM.shopping.innerHTML = "";
        renderNavShop(state.carts.totalPrice, -1);
        console.log(state.carts.carts);
        clearThanhToan();
        renderBccsp();
        Swal.fire({
            icon: 'success',
            title: 'Thành công!',
            text: 'Đặt hàng thành công.',
            showConfirmButton: true
})
    }
    })

// Search 
const searchController = async (e) => {
    const value = e.target.value;
    if(value){
        const newResult = [];
        const search = new Search();
        const result  = await search.getData();
        newResult.push(...result)
      
        const findMatches = search.findMatches(value, newResult)
        displayMatches(value , findMatches);
    }
}
DOM.search.addEventListener('change', searchController);
DOM.search.addEventListener('keyup', searchController);
console.log("Van ly doc hanh van ly sau");