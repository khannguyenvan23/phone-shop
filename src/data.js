[
  {
    "id": "1",
    "name": "Samsung Galaxy Note 10+",
    "image": "https://cdn.tgdd.vn/Products/Images/42/218355/samsung-galaxy-note-20-051120-101146-400x460.png",
    "description": "Camera đứng đầu thế giới",
    "price": 17000000,
    "rating": 60,
    "type": "samsung",
    "inventory": 20
  },
  {
    "id": "2",
    "name": "OPPO Reno3",
    "image": "https://cdn.tgdd.vn/Products/Images/42/213591/oppo-reno3-trang-600x600-400x400.jpg",
    "description": "Nhiếp ảnh, quay phim đỉnh với cụm camera chất",
    "price": 7490000,
    "rating": 50,
    "type": "oppo",
    "inventory": 30
  },
  {
    "id": "3",
    "name": "OPPO A52",
    "image": "https://cdn.tgdd.vn/Products/Images/42/220649/oppo-a52-black-400-400x460.png",
    "description": "Cấu hình mạnh nhiều tính năng hấp dẫn",
    "price": 5990000,
    "rating": 40,
    "type": "oppo",
    "inventory": 70
  },
  {
    "id": "4",
    "name": "Samsung Galaxy A21s",
    "image": "https://cdn.tgdd.vn/Products/Images/42/219314/samsung-galaxy-a21s-055620-045659-400x400.jpg",
    "description": "Thiết kế cao cấp, cảm biến vân tay ở mặt lưng",
    "price": 4890000,
    "rating": 70,
    "type": "samsung",
    "inventory": 80
  },
  {
    "id": "5",
    "name": "Iphone  11 Pro Max 64GB",
    "image": "https://cdn.tgdd.vn/Products/Images/42/200533/iphone-11-pro-max-green-400x400.jpg",
    "description": "Hiệu năng đè bẹp mọi đối thủ",
    "price": 33990000,
    "rating": 90,
    "type": "apple",
    "inventory": 75
  },
  {
    "id": "6",
    "name": "Vsmart Actice 3",
    "image": "https://cdn.tgdd.vn/Products/Images/42/217438/vsmart-active-3-6gb-emerald-green-600x600-400x400.jpg",
    "description": "Sang trọng với mặt lưng kính, hiệu ứng gradient thời thượng",
    "price": 3990000,
    "rating": 50,
    "type": "vsmart",
    "inventory": 30
  },
  {
    "id": "7",
    "name": "OPPO Reno3",
    "image": "https://cdn.tgdd.vn/Products/Images/42/213591/oppo-reno3-trang-600x600-400x400.jpg",
    "description": "Nhiếp ảnh, quay phim đỉnh với cụm camera chất",
    "price": 7490000,
    "rating": 60,
    "type": "oppo",
    "inventory": 60
  },
  {
    "id": "8",
    "name": "iPhone 11 128GB",
    "image": "https://cdn.tgdd.vn/Products/Images/42/210644/iphone-11-128gb-green-400x400.jpg",
    "description": "Nâng cấp mạnh mẽ về cụm camera",
    "price": 23990000,
    "rating": 60,
    "type": "apple",
    "inventory": 40
  },
  {
    "id": "9",
    "name": "Xiaomi Redmi Note 8 Pro (6GB/128GB)",
    "image": "https://cdn.tgdd.vn/Products/Images/42/214418/xiaomi-redmi-note-8-pro-6gb-128gb-white-400x400.jpg",
    "description": "smartphone hứa hẹn sẽ gây bão trong thời gian tới",
    "price": 5690000,
    "rating": 45,
    "type": "xiaomi",
    "inventory": 40
  },
  {
    "id": "10",
    "name": "Realme C3 (3GB/64GB)",
    "image": "https://cdn.tgdd.vn/Products/Images/42/225176/realme-c3-64gb-263620-023637-200x200.jpg",
    "description": "Bắt trọn mọi chi tiết với bộ 3 camera AI 12 MP",
    "price": 3390000,
    "rating": 80,
    "type": "xiaomi",
    "inventory": 70
  },
  {
    "id": "11",
    "name": "Samsung Galaxy Z Flip",
    "image": "https://cdn.tgdd.vn/Products/Images/42/213022/samsung-galaxy-z-flip-den-600x600-400x400.jpg",
    "description": "Đột phá với thiết kế màn hình gập",
    "price": 36000000,
    "rating": 70,
    "type": "samsung",
    "inventory": 50
  },
  {
    "id": "12",
    "name": "iPhone Xs Max 256GB",
    "image": "https://cdn.tgdd.vn/Products/Images/42/190322/iphone-xs-max-256gb-white-400x400.jpg",
    "description": "Hiệu năng đỉnh cao cùng chip Apple A12",
    "price": 38990000,
    "rating": 43,
    "type": "apple",
    "inventory": 90
  },
  {
    "id": "13",
    "name": "Xiaomi Mi Note 10 Lite",
    "image": "https://cdn.tgdd.vn/Products/Images/42/220851/xiaomi-mi-note-10-lite-trang-600x600-600x600.jpg",
    "description": "Thời trang và thanh thoát với màn hình tràn viền 3D",
    "price": 9990000,
    "rating": 43,
    "type": "xiaomi",
    "inventory": 90
  },
  {
    "id": "14",
    "name": "Xiaomi Redmi Note 9 Pro (6GB/128GB)",
    "image": "https://cdn.tgdd.vn/Products/Images/42/221820/xiaomi-redmi-note-9-pro-128gb-white-600x600-200x200.jpg",
    "description": "Thiết kế bóng bẩy, cao cấp",
    "price": 6990000,
    "rating": 60,
    "type": "xiaomi",
    "inventory": 90
  }
]